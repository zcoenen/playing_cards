// C++ Lab Exercise 2 - Playing Cards
// Zach Coenen
// 1/31/2022

//header files
#include <iostream>
#include <conio.h>
#include <string>
#include <vector>

using std::vector;
using std::string;


enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Clubs, Diamonds, Hearts, Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};


int main()
{



}